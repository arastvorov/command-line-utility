#!/usr/bin/env node
const executeCommand = require('./src/executeCommand')
const { print } = require('./src/helpers')

const filesToShow = executeCommand()
print(filesToShow)

module.exports = executeCommand
