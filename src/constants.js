
const FILTER_FLAGS = ['-hf', '-vf']
const SORT_FLAGS = ['-t']
const RENDER_FLAGS = ['-l']
const SUPPORTED_FLAGS = [...FILTER_FLAGS, ...SORT_FLAGS, ...RENDER_FLAGS]

module.exports = {
  FILTER_FLAGS,
  SORT_FLAGS,
  RENDER_FLAGS,
  SUPPORTED_FLAGS
}
