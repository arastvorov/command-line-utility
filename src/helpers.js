const fs = require('fs')
const regex = /^\..*/

function isHidden (filename) {
  return regex.test(filename)
};

function sortBySupportedValues (inputValues, supportedValues) {
  return inputValues.sort((a, b) => supportedValues.indexOf(a) - supportedValues.indexOf(b))
}

function hasOnlyValidOptions (inputValues, supportedValues) {
  return inputValues.every(item => supportedValues.includes(item))
}

function getFilesStats (filenames, directory) {
  return filenames.map(filename => ({
    stat: fs.statSync(`${directory}/${filename}`),
    filename
  }))
}

function print (data = []) {
  data.forEach(item => console.log(item))
}

function handleFilesByFlag (filesnames, flag, directoryPath) {
  switch (flag) {
    case '-hf':
      return filesnames.filter(isHidden)
    case '-vf':
      return filesnames.filter(file => !isHidden(file))
    case '-t':
      return getFilesStats(filesnames, directoryPath)
        .sort((a, b) => a.stat.mtime < b.stat.mtime)
        .map(item => item.filename)
    case '-l':
      return getFilesStats(filesnames, directoryPath)
        .map(({ filename, stat }) => `Filename: ${filename}, size in bytes: ${stat.size}, last modified date: ${stat.mtime}`)
    default:
      return filesnames
  }
}

function handleFiles (files, flags, directoryPath) {
  return flags.reduce((accum, flag) => {
    return handleFilesByFlag(accum, flag, directoryPath)
  }, files)
}

module.exports = {
  isHidden,
  sortBySupportedValues,
  hasOnlyValidOptions,
  getFilesStats,
  print,
  handleFilesByFlag,
  handleFiles
}
