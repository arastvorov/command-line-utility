const fs = require('fs')
const {
  sortBySupportedValues,
  hasOnlyValidOptions,
  handleFiles,
} = require('./helpers');
const { SUPPORTED_FLAGS } = require('./constants')

function executeCommand () {
  const directoryPath = process.cwd()
  const [,, ...args] = process.argv

  if (args.length > 0 && !hasOnlyValidOptions(args, SUPPORTED_FLAGS)) {
    throw new Error(`Provided arguments for myls command are not supported\nPlease, use these ${SUPPORTED_FLAGS.join(', ')}.`)
  }

  try {
    const files = fs.readdirSync(directoryPath)
    const sortedArgs = sortBySupportedValues(args, SUPPORTED_FLAGS)
    return args.length === 0 ? files : handleFiles(files, sortedArgs, directoryPath)
  } catch (error) {
    if (error.code === 'ENOENT') {
      console.log('No such file or directory!')
    } else {
      throw error
    }
  }
}

module.exports = executeCommand;
