const mockFs = require('mock-fs')
const mockArgv = require('mock-argv')
const executeCommand = require('../src/executeCommand')

describe('Command-line utility execution tests', () => {
  beforeEach(function () {
    const cwd = process.cwd()
    const fakeDir = `/${cwd}`

    mockFs({
      [fakeDir]: {
        'testFile1.txt': mockFs.file({
          content: 'Test content',
          mtime: new Date().setDate(1)
        }),
        '.hidden': mockFs.file({
          content: 'Test content ',
          mtime: new Date().setDate(2)
        }),
        'testFile2.txt': mockFs.file({
          content: 'Test content second',
          mtime: new Date().setDate(3)
        })
      }
    }, { createCwd: false })
  })

  afterEach(function () {
    mockFs.restore()
  })

  it('should list all the files in provided directory', () => {
    const filesToRender = executeCommand()
    expect(filesToRender).toEqual(['.hidden', 'testFile1.txt', 'testFile2.txt'])
  })

  it('should list only system files in provided directory', (callback) => {
    mockArgv(['-hf'], () => {
      const filesToRender = executeCommand()
      expect(filesToRender).toEqual(['.hidden'])
      callback()
    })
  })

  it('should list visible files in provided directory', (callback) => {
    mockArgv(['-vf'], () => {
      const filesToRender = executeCommand()
      expect(filesToRender).toEqual(['testFile1.txt', 'testFile2.txt'])
      callback()
    })
  })

  it('should list all the files in provided directory sorted by modification time', (callback) => {
    mockArgv(['-t'], () => {
      const filesToRender = executeCommand()
      expect(filesToRender).toEqual(['testFile2.txt', '.hidden', 'testFile1.txt'])
      callback()
    })
  })

  it('should list visible files in provided directory sorted by modification time', (callback) => {
    mockArgv(['-t', '-vf'], () => {
      const filesToRender = executeCommand()
      expect(filesToRender).toEqual(['testFile2.txt', 'testFile1.txt'])
      callback()
    })
  })

  it('should throw the error if provided wrong argument', (callback) => {
    mockArgv(['-x'], () => {
      try {
        executeCommand()
      } catch (error) {
        const errorMessage = 'Provided arguments for myls command are not supported\nPlease, use these -hf, -vf, -t, -l.'

        expect(error.message).toBe(errorMessage)
        callback()
      }
    })
  })
})
