# Command-line utility

Command-line utility package provides bush command `myls` - it lists files and directories where this commands has been invoked.

## Install

You can use `myls` command after installing this package globally `npm i command-line-util -g`;

Or you can clone this repo and run `npm link`;

## How to use

`myls` by default lists all the files in the current working directory.

Several flags are available:

  - `-hf` lists system files;

  - `-vf` lists ordinary files;

  - `-t` sort files by last modification date;

  - `-l` displays filename, size in bytes and last modification date;

  ## Testing

  You can run test via `npm run test`.

  ## Linter

  [StandardJS linter](https://standardjs.com/) is used for code formatting.

  To prettify your code run `npm run pretty`.